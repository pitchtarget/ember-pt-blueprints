/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: '<%= dasherizedPackageName %>',
    podModulePrefix: '<%= dasherizedPackageName %>/pods',
    environment: environment,
    baseURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {

  }

  ENV['ember-simple-auth'] = {
    authenticationRoute: 'sign-in',
    routeAfterAuthentication: 'session.session.fb-login',
    // routeIfAlreadyAuthenticated: 'home' // it doesn't work well with engines
  };

  ENV.ptApi = {
    baseUrl: 'https://api-production.pitchtarget.com',
    clientId: 'drfwy7quc9behg9l2hq02dbcn8hvw51',
    defaultProviderLoginRoute: 'session.session.fb-login'
  };

  ENV.FB = {
    version: 'v3.1',
    appId: '668664849828937'
  };

  return ENV;
};
