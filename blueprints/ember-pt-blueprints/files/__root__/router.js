import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('session', function() {
    this.mount('login-engine', {as: 'session', path: '/'});
  });
  this.route('home');
});

export default Router;
