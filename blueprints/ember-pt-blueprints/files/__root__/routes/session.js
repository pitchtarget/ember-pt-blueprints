import Ember from 'ember';

export default Ember.Route.extend({
  beforeModel(transition) {
    this._super(...arguments);

    if (transition.intent.url === '/session') {
      this.transitionTo('session.session.sign-in');
    }
  }
});
