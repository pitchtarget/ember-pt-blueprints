import Ember from 'ember';
import FbAuthenticatedRouteMixin from 'ember-pt-config/mixins/routes/fb-authenticated';

export default Ember.Route.extend(FbAuthenticatedRouteMixin, {
  fb: Ember.inject.service(),
  session: Ember.inject.service(),

  beforeModel() {
    if (!this.get('session.isAuthenticated')) {
      return this.transitionTo('session.session.sign-in');
    }
    this._super(...arguments);
  },

  model() {
    return this.get('fb').api('/me');
  }
});
