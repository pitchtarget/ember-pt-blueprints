/*jshint node:true*/

module.exports = {
  description: '',
  normalizeEntityName: () => {},
  afterInstall: function(options) {
    return this.addBowerPackagesToProject([
      { name: 'moment'},
      { name: 'font-awesome'}
    ]).then(() => {
      return this.addAddonsToProject({
        packages: [
          { name: 'login-engine'},
          { name: 'ember-cli-sass-pods'},
          { name: 'ember-cli-facebook-js-sdk'},
          { name: 'ember-cli-material-design-icons'},
          { name: 'ember-pt-config'},
          // { name: 'ember-pt-components', target: 'git+ssh://git@bitbucket.org:pitchtarget/ember-pt-components.git#feature/ui-components'},
          { name: 'ember-engines', target: '0.2.7'},
          { name: 'ember-simple-auth', target: '1.1.0'},
          { name: 'torii', target: '0.8.0'},
          { name: 'ember-notify'},
          { name: 'ember-cli-sass'},
          { name: 'ember-font-awesome'},
          { name: 'ember-moment'},
          { name: 'ember-i18n'}
        ]
      });
    })
    // .then(() => {
    //   return this.addPackagesToProject([
    //   ]);
    // })
  }
};
